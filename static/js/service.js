app.factory("getDetailsService", function($http) {
    return {
        details: function() {
            return $http.get("js/vendor/MOCK_DATA.json").then(function(response) {
                return response.data;
            });
        }
    }
});
