app.controller("homeCtrl",function($scope,$location,$timeout,getDetailsService){
  
    $scope.data=[];
    $scope.images=['images/1.jpg','images/2.jpg','images/3.jpg','images/4.jpg','images/5.jpg','images/6.jpg'];
    $scope.totalItems=0;
    $scope.modale={header:"ads"};

    getDetailsService.details().then(function(res){
        $scope.data = res;
        $scope.totalItems = $scope.data.length;
      }).catch(function(data){
        console.log('Unable to fetch data');
      });
    
      $scope.viewby = 10;      
      $scope.currentPage = 1;
      $scope.itemsPerPage = $scope.viewby;
      $scope.maxSize = 5; //Number of pager buttons to show
    
      $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
      };
    
      $scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.currentPage);
      };
    
    $scope.setItemsPerPage = function(num) {
      $scope.itemsPerPage = num;
      $scope.currentPage = 1; //reset to first page
    }

    $scope.modalData = function(index) {
       $scope.modale.header=$scope.data[index].header;
       $scope.modale.text=$scope.data[index].text;
       $timeout(function () {
            jQuery("#modalHeader").html($scope.modale.header);
            jQuery("#modalBody").html($scope.modale.text);
    }, 200);       
      }

     $scope.deleteData = function(index) {
            $scope.data.splice(index,1);
       }

    $scope.printPage = function() {
        window.print();
   }
});
