var app=angular.module("infobip",['ngRoute','ngStorage','ui.bootstrap']);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "html/gallery.html",
        controller: "homeCtrl"
    })
    .when("/profile", {
        templateUrl : "html/gallery.html",
        controller: function($window,$location){
        $window.location.href="http://mohitagarwal.xyz";
      }
    })
    .otherwise({
      redirectTo: "/"
    });
});
